# README
Application reverses string given in data parameter.
Provide string you want to reverse in a browser e.g
http://localhost:3000/?data='Loremipsum'
Application returns json format with original string in data and reversed string in message.
```
{
  original: {
    data: "Loremipsum"
  }, 
  message: "muspimeroL"
}
```
