class ApiController < ApplicationController

  def show
    request.format = :json
    if params[:data]
      text = params[:data].reverse
      render json: { original: { data: params[:data] }, message: text }
    else
      render json: { error: "data parameter is required"}, status: 400
    end
  end
end
