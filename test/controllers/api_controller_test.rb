require 'test_helper'

class ApiControllerTest < ActionDispatch::IntegrationTest

  test "should return reversed string" do
    params = { data: 'Lorem ipsum'}
    get root_path, params: params
    parsed_json = JSON.parse(response.body)
    assert_match parsed_json['original']['data'], params[:data]
    assert_match parsed_json['message'], params[:data].reverse
  end

  test "should return error message" do
    get root_path
    parsed_json = JSON.parse(response.body)
    assert_match parsed_json['error'], 'data parameter is required'
    assert_equal 400, status
  end
end
